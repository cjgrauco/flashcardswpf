﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace FlashCardsWPF
{
    /// <summary>
    /// Interaction logic for CreateCard.xaml
    /// </summary>
    public partial class CreateCard : Window
    {
        private int cardID;

        private Deck deck;


        public CreateCard(Deck deck, int cardID)
        {
            this.deck = deck;
            this.cardID = cardID;

            InitializeComponent();
        }
        

        private void addCard(object sender, RoutedEventArgs e)
        {
            //TODO add functionality for label parsing and add labels to array.
            if (string.IsNullOrWhiteSpace(textBoxFront.Text) && string.IsNullOrWhiteSpace(textBoxBack.Text))
            {
                labelFrontBackEmpty.Content = "Front and back of card is empty.";
                labelFrontBackEmpty.Visibility = Visibility.Visible;

            } else if (string.IsNullOrWhiteSpace(textBoxBack.Text))
            {
                labelFrontBackEmpty.Content = "Back of card is empty.";
                labelFrontBackEmpty.Visibility = Visibility.Visible;

            } else if(string.IsNullOrWhiteSpace(textBoxFront.Text)){
                labelFrontBackEmpty.Content = "Front of card is empty.";
                labelFrontBackEmpty.Visibility = Visibility.Visible;

            }else
            {
                if(labelFrontBackEmpty.Visibility == Visibility.Visible)
                {
                    labelFrontBackEmpty.Visibility = Visibility.Hidden;
                }
                Card newCard = new Card(textBoxFront.Text, textBoxBack.Text, textBoxLabels.Text, cardID, deck.getDeckID());
                deck.addCard(newCard);

                writeToDB(newCard); 


                // Close window
                this.Close();
                 
                 
            }
        }

        private void writeToDB(Card newCard)
        {
            //TODO new card to database
            SQLiteConnection dbCon = new SQLiteConnection(@"Data Source=C:\Users\Johan G\Documents\Visual Studio 2015\Projects\FlashCardsWPF\FlashCardsWPF\db.sqlite;Version=3;");

            dbCon.Open();



            string sql = "insert into Card (cardID, front, back, deckID, labels) values (" + newCard.getCardID() + ", \'" + newCard.getFront() + "\', \'" + newCard.getBack() + "\', " + newCard.getDeckID() + ", \'" + newCard.getLabels() + "\')";
            
            if(Globals.debugMode == 1)
            {
                Console.WriteLine("Card SQL insert: " + sql);
            }

            SQLiteCommand cmd = new SQLiteCommand(sql, dbCon);
            cmd.ExecuteNonQuery();

            dbCon.Close(); 
            
        }
    }
}
