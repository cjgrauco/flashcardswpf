﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;



namespace FlashCardsWPF

/*
+-----------------------+      +---------------------+     +-----------------------+
|         Card          |      |        Deck         |     |         Info          |
+-----------------------+      +---------------------+     +-----------------------+
|cardID INTEGER         |      |deckID INTEGER       |     |version REAL           |
|                       |      |                     |     |                       |
|front TEXT             |      |deckName TEXT        |     |deckCount INTEGER      |
|                       |      |                     |     |                       |
|back TEXT              |      |PQ(deckID)           |     |PQ(version)            |
|                       |      |                     |     |                       |
|deckID INTEGER         |      |                     |     |                       |
|                       |      +---------------------+     +-----------------------+
|labels TEXT            |
|                       |
|PQ(cardID, deckID)     |
+-----------------------+

*/
{

    public static class Globals
    {
        public static int debugMode = 1;

    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //TODO Delete card
        //TODO Delete deck
        //TODO View deck
        //TODO Add card to existing deck
        //TODO make static class with database logic.

        private float version;
        private Deck[] decks;
        private int deckCount = 0;
        public MainWindow()
        {
            decks = new Deck[100];
            readInfo();
            readDecks();
            readCards();

            Console.WriteLine("------> DECKCOUNT = " + deckCount);

            InitializeComponent();
            Title = "Flash Cards version " + version;            
        }

        private void createDeckPressed(object sender, RoutedEventArgs e)
        {
            CreateDeck cd = new CreateDeck(deckCount);
            deckCount++;
            cd.Show();
        }


        /*
         * Reads previously made decks/cards from the database into memory.
         */
        private void readDecks()
        {
            SQLiteConnection db = DBLogic.dbOpen();

            string sql = "select * from Deck";

            SQLiteCommand command = new SQLiteCommand(sql, db);
            SQLiteDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                if(Globals.debugMode == 1)
                {
                    Console.WriteLine("deckID: " + dataReader["deckID"] + " deckName: " + dataReader["deckName"]);
                }

                Deck newDeck = new Deck(dataReader["deckName"].ToString(), dataReader.GetInt32(dataReader.GetOrdinal("deckID")));
                decks[newDeck.getDeckID()] = newDeck;

            }
            DBLogic.dbClose(db);

        }


        private void readCards()
        {

            SQLiteConnection db = DBLogic.dbOpen();

            string sql = "select * from Card";

            SQLiteCommand command = new SQLiteCommand(sql, db);
            SQLiteDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            { 
                string front = dataReader["front"].ToString();
                string back = dataReader["back"].ToString();
                int cardID = dataReader.GetInt32(dataReader.GetOrdinal("cardID"));
                int deckID = dataReader.GetInt32(dataReader.GetOrdinal("deckID"));
                string labels = "";

                if (!dataReader.IsDBNull(5))
                {
                    labels = dataReader["labels"].ToString();
                }



                if (Globals.debugMode == 1)
                {
                    Console.WriteLine("cardID: " + cardID + " deckID: " + deckID);
                    Console.WriteLine("front: " + dataReader["front"]);
                    Console.WriteLine("back: " + dataReader["back"]);
                }


                Card newCard = new Card(front, back, labels, cardID, deckID);
                decks[deckID].addCard(newCard); 
            }


            DBLogic.dbClose(db);

        }

       





        private void readInfo()
        {
            SQLiteConnection db = DBLogic.dbOpen();

            string sql = "select * from Info";

            SQLiteCommand command = new SQLiteCommand(sql, db);
            SQLiteDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                if (Globals.debugMode == 1)
                {
                    Console.WriteLine("Version: " + dataReader["version"] + " Deck count: " + dataReader["deckCount"]);
                }

                version = dataReader.GetFloat(dataReader.GetOrdinal("version"));
                deckCount = dataReader.GetInt32(dataReader.GetOrdinal("deckCount"));
            }

            DBLogic.dbClose(db);
        }



    }
}
