﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace FlashCardsWPF
{
    public static class DBLogic
    {
        public static SQLiteConnection dbOpen()
        {
            try
            {
                SQLiteConnection dbCon = new SQLiteConnection(@"Data Source=C:\Users\Johan G\Documents\Visual Studio 2015\Projects\FlashCardsWPF\FlashCardsWPF\db.sqlite;Version=3;");

                if (Globals.debugMode == 1)
                {
                    Console.WriteLine("--->   Connection to database opened.");
                }
                dbCon.Open();
                return dbCon;

            }
            catch (ArgumentException e)
            {
                Console.WriteLine("--->   Connection to database failed: ");
                Console.WriteLine(e);
                Console.WriteLine("");

            }
            return null;

        }


        public static void dbClose(SQLiteConnection con)
        {
            if (Globals.debugMode == 1)
            {
                Console.WriteLine("--->   Connection to database closed.");
            }


            con.Close();

        }

        public static void executeStatement()
        {

        }

    }
}