﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace FlashCardsWPF
{
    /// <summary>
    /// Interaction logic for CreateDeck.xaml
    /// </summary>
    public partial class CreateDeck : Window
    {
        private int deckID;
        private int cardCount;
        private Deck deck;
        private SQLiteConnection dbCon; 

        public CreateDeck(int deckID)
        {
            this.deckID = deckID;
            cardCount = 0;
            InitializeComponent();
            //comboBoxDeck.Items.Insert(0, "Select card");
        }

        private void createCardPressed(object sender, RoutedEventArgs e)
        {
            CreateCard cc = new CreateCard(deck, cardCount);
            cardCount++;
            cc.Show();
        }

        private void textBoxDeckName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void createDeckPressed(object sender, RoutedEventArgs e)
        {
            //TODO check if deck name already exists in db
            // update number of decks in db
            if(!string.IsNullOrWhiteSpace(textBoxDeckName.Text))
            {
                deck = new Deck(textBoxDeckName.Text, deckID);

                //write deck to database
                writeToDB(deck);

                comboBoxDeck.Visibility = Visibility.Visible;
                labelDropdownCards.Visibility = Visibility.Visible;
                buttonCreateCard.Visibility = Visibility.Visible;

                if(labelDeckEmptyOrExists.Visibility == Visibility.Visible)
                {
                    labelDeckEmptyOrExists.Visibility = Visibility.Hidden;
                }
            }else
            {
                labelDeckEmptyOrExists.Content = "Deck name is empty or already exists.";
                labelDeckEmptyOrExists.Visibility = Visibility.Visible;
            }
        }

        private void comboBoxDeck_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        
        private void writeToDB(Deck deck)
        {
            //TODO update Info: deckCount
            dbCon = DBLogic.dbOpen();
           



            string sql = "insert into Deck (deckID, deckName) values (" + deck.getDeckID() +  ", \'" + deck.getName() + "\')";
            

            if (Globals.debugMode == 1)
            {
                Console.WriteLine("Deck SQL insert: " + sql);
            }

            SQLiteCommand cmd = new SQLiteCommand(sql, dbCon);
            cmd.ExecuteNonQuery();


            int newDeckCount = deck.getDeckID() + 1;
            sql = "update Info set deckCount = " + newDeckCount;

            cmd = new SQLiteCommand(sql, dbCon);
            cmd.ExecuteNonQuery(); 
            
            DBLogic.dbClose(dbCon);
        }


        private Boolean isDeckUnique()
        {

            dbCon = DBLogic.dbOpen();
            string sql = "select * from Card";

            SQLiteCommand cmd = new SQLiteCommand(sql, dbCon);

            return false;
        }
    }
}
