﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsWPF
{
    public class Deck 
    {
        private int deckID;
        private string name;
        private Card[] cards;
        private int cardCount;


        public Deck(string name, int deckID)
        {
            this.name = name; 
            this.deckID = deckID;
            cardCount = 0; 
            cards = new Card[300];

            if(Globals.debugMode == 1)
            {
                Console.WriteLine("New deck created ->  deckName: " + name + " deckID: " + deckID);

            }

        }



        public void addCard(Card card)
        {
            cards[cardCount] = card;
            
            if(Globals.debugMode == 1)
            {
                Console.WriteLine("New Card added to deck: " + name + "  " + " DeckID: " + deckID + " CardID: " + cardCount);
                card.printInfo(); 
            }
            cardCount++;
        }

        public string getName()
        {
            return name;
        }


        public int getDeckID()
        {
            return deckID;
        }

        public void printInfo()
        {
            Console.WriteLine("---> Deck Info <---");
            Console.WriteLine("Name: " + name);
            Console.WriteLine("DeckID: " + deckID);
            Console.WriteLine("Cards in " + name + " deck:");

            for(int i = 0; i > cards.Length; i++)
            {
                if (cards[i] == null)
                {
                    break;
                }
                else
                {
                    cards[i].printInfo();
                }
            }
        }
    }
}
