﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashCardsWPF
{
    public class Card
    {
        private string front, back, labels;
        private string[] labelsSplit;
        private int cardID;
        private int deckID;

        public Card(string front, string back, string labels, int cardID, int deckID)
        {

            this.front = front;
            this.back = back;
            this.labels = labels;
            this.cardID = cardID;
            this.deckID = deckID;

            if(Globals.debugMode == 1)
            {
                printInfo();
            }

            //TODO handle labels 
            if (!string.IsNullOrWhiteSpace(labels))
            {

                
            }
        }

        public string getFront()
        {
            return front;
        }

        public string getBack()
        {
            return back;
        }

        public int getCardID()
        {
            return cardID;
        }

        public int getDeckID()
        {
            return deckID;
        }


        public string getLabels()
        {

            return labels;
        }


        public void printInfo()
        {
            
            Console.WriteLine("Front: " + front);
            Console.WriteLine("Back: " + back);
            Console.WriteLine("Id: " + cardID);
            Console.WriteLine("Labels: " + labels);
            Console.WriteLine("");
        }


        private void splitLabels(){
            char[] delimChars = {' ', ','};

            labelsSplit = labels.Split(delimChars);

            if (Globals.debugMode == 1)
            {
                foreach (string s in labelsSplit)
                {
                    Console.WriteLine();
                }

            }
        }
    }
}
